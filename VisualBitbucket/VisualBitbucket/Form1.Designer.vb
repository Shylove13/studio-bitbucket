﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnRepo = New System.Windows.Forms.Button()
        Me.lblRepos = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnRepo
        '
        Me.btnRepo.Location = New System.Drawing.Point(272, 148)
        Me.btnRepo.Name = "btnRepo"
        Me.btnRepo.Size = New System.Drawing.Size(196, 37)
        Me.btnRepo.TabIndex = 0
        Me.btnRepo.Text = "Repository"
        Me.btnRepo.UseVisualStyleBackColor = True
        '
        'lblRepos
        '
        Me.lblRepos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblRepos.Location = New System.Drawing.Point(330, 239)
        Me.lblRepos.Name = "lblRepos"
        Me.lblRepos.Size = New System.Drawing.Size(100, 23)
        Me.lblRepos.TabIndex = 1
        Me.lblRepos.Text = "Repo"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.lblRepos)
        Me.Controls.Add(Me.btnRepo)
        Me.Name = "Form1"
        Me.Text = "Visual & Bitbucket"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnRepo As Button
    Friend WithEvents lblRepos As Label
End Class
